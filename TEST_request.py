import requests
import re
import json
import os
import urllib3
import time
from urllib.parse import quote
from gasbuddy_imgop import Distinguish
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

in_dir = os.path.join(os.path.dirname(__file__), "in_dir")
out_dir = os.path.join(os.path.dirname(__file__), "out_dir")

if not os.path.exists(in_dir):
    os.makedirs(in_dir)

if not os.path.exists(out_dir):
    os.makedirs(out_dir)


img_headers = {
    "Host": "captcha.su.baidu.com",
    "Connection": "keep-alive",
    "Sec-Fetch-Mode": "no-cors",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
    "Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
    "Sec-Fetch-Site": "cross-site",
    "Referer": "https://www.gasbuddy.com/",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9",
}

headers = {
    "Host": "captcha.su.baidu.com",
    "Connection": "keep-alive",
    "Sec-Fetch-Mode": "no-cors",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
    "Accept": "*/*",
    "Sec-Fetch-Site": "cross-site",
    "Referer": "https://www.gasbuddy.com/",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9"
}

base_obj_url = "https://captcha.su.baidu.com/session_cb?pub={}&callback="
base_img_url = "https://captcha.su.baidu.com/image?session={}&pub={}"

pub = "377e4907e1a3b419708dbd00df9e8f79"


# 获取验证码图片url的方法，每个url只能访问一次
def get_img_url():
    _url = base_obj_url.format(pub)
    response = requests.get(_url, headers=headers,verify=False)
    obj_json_info = re.search('callback\((.*?)\)$', response.text, re.S | re.I)
    if obj_json_info:
        json_info = obj_json_info.group(1).strip()
        json_info = json.loads(json_info)
        sessionstr = json_info.get("sessionstr")
        img_url = base_img_url.format(sessionstr, pub)
        return img_url, sessionstr

# 用于测试100 个图片中有几个能识别正确
def test():
    count = 100
    for i in range(count):
        try:
            print("started Distinguish %s" % str(i + 1))
            img_url, sessionstr = get_img_url()
            response = requests.get(img_url, headers=img_headers, verify=False)
            in_path = os.path.join(in_dir,"%s.bmp" % time.time())
            with open(in_path, "wb") as f:
                f.write(response.content)

            info = Distinguish(in_path)  # 识别后返回结果
            words_result = info.get("words_result")
            if not words_result:
                in_path = os.path.join(out_dir, "error%s.bmp" % str(i+1))
                with open(in_path, "wb") as f:
                    f.write(response.content)
                continue
            _result = words_result[0]
            words = _result.get("words")
            in_path = os.path.join(out_dir, "%s.bmp" % words)
            with open(in_path, "wb") as f:
                f.write(response.content)
        except Exception as ex:
            print("Exception in main_test,ex: "+ str(ex))

        print("finished Distinguish %s" % str(i + 1))


# 访问测试，模拟访问网站，输入验证到成功的过程
# 函数循环访问有100次，做验证码识别。若识别成功跳出循环，返回验证成功的cookies
# 验证验证码url：https://www.gasbuddy.com/cdn-cgi/l/chk_captcha?s={}&id={}&captcha_challenge_field={}&manual_captcha_challenge_field={}
# 相关参数有四个：
# 1 s，2 id 主页中的隐藏参数
# 3 captcha_challenge_field，验证码图片的标识参数
# 4 manual_captcha_challenge_field 验证码内容

def request_TEST():
    obj_url = "https://www.gasbuddy.com/"  # 主页 url
    obj_headers = { # 主页 headers
        "Host": "www.gasbuddy.com",
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9",
    }
    response = requests.get(obj_url, headers=obj_headers, verify=False)  # 访问主页
    sour = response.text
    iRetry = 1
    success = True
    while iRetry <= 100 and success:
        try:
            if iRetry  > 10 :
                print("started Distinguish, iRetry: " + str(iRetry))
            # 首次访问 获取隐藏参数一/ 二 ##检验失败的验证码会再一次获取隐藏参数一/ 二
            obj_s = re.search('name="s"\s*value="(.*?)">', sour, re.S | re.I)  # 验证请求参数一s
            if obj_s:
                s = obj_s.group(1).strip()
                s = quote(s,"utf-8")

            obj_id = re.search('data-type="normal"\s*data-ray="(.*?)"\s*async></script>', sour, re.S | re.I) # 验证请求参数二 id
            if obj_id:
                _id = obj_id.group(1).strip()

            img_url, sessionstr = get_img_url() # 获取验证码 url 及验证码标识 sessionstr
            response = requests.get(img_url, headers=img_headers, verify=False)
            in_path = os.path.join(in_dir, "%s.bmp" % time.time())
            with open(in_path, "wb") as f:
                f.write(response.content)

            info = Distinguish(in_path)  # 识别后返回结果

            words_result = info.get("words_result")
            if not words_result:  # 验证请求无返回值，验证失败 ，再次访问主页
                print("words_result is null" , info)
                iRetry += 1
                response = requests.get(obj_url, headers=obj_headers, verify=False)
                sour = response.text
                continue
            # 验证请求有返回值，判断返回值是否正确
            _result = words_result[0]
            words = _result.get("words")
            words = str(words).replace(" ", "") # 提取验证码识别值
            # 构造检验验证码的url
            _url = "https://www.gasbuddy.com/cdn-cgi/l/chk_captcha?s={}&id={}&captcha_challenge_field={}&manual_captcha_challenge_field={}"\
                .format(s, _id, sessionstr, words)
            response = requests.get(_url, headers=obj_headers,timeout=30, verify=False)
            sour = response.text
            if "输入验证码，可以浏览" in sour:  # 用于判断检验是否成功，检验失败的url会跳转至主页
                iRetry += 1
                continue
            else: # 检验成功 ，打印日志，跳出循环，返回cookies
                success  = False
                print("验证码识别成功")
                _cookies = response.request._cookies.get_dict()
                print(_cookies)
                return _cookies

        except Exception as ex:
            print("Exception in main_test,ex: " + str(ex))

        iRetry += 1




if __name__ == '__main__':
    request_TEST()