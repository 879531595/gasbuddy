from PIL import Image, ImageDraw
import cv2
from baiduorc import *
import numpy as np

'''baiduorc 百度云图像识别服务，详情功能见官网'''

obj = (98,114,178)
bai = (255, 255, 255)
t2val = {}

# 去噪时 需要的一些函数
def twoValue(image, G):
    for y in range(0, image.size[1]):
        for x in range(0, image.size[0]):
            g = image.getpixel((x, y))
            if g > G:
                t2val[(x, y)] = 1
            else:
                t2val[(x, y)] = 0

def clearNoise(image, N, Z):
    for i in range(0, Z):
        t2val[(0, 0)] = 1
        t2val[(image.size[0] - 1, image.size[1] - 1)] = 1

        for x in range(1, image.size[0] - 1):
            for y in range(1, image.size[1] - 1):
                nearDots = 0
                L = t2val[(x, y)]
                if L == t2val[(x - 1, y - 1)]:
                    nearDots += 1
                if L == t2val[(x - 1, y)]:
                    nearDots += 1
                if L == t2val[(x - 1, y + 1)]:
                    nearDots += 1
                if L == t2val[(x, y - 1)]:
                    nearDots += 1
                if L == t2val[(x, y + 1)]:
                    nearDots += 1
                if L == t2val[(x + 1, y - 1)]:
                    nearDots += 1
                if L == t2val[(x + 1, y)]:
                    nearDots += 1
                if L == t2val[(x + 1, y + 1)]:
                    nearDots += 1

                if nearDots < N:
                    t2val[(x, y)] = 1

# 图片存储函数
def saveImage(filename, size):
    image = Image.new("1", size)
    draw = ImageDraw.Draw(image)

    for x in range(0, size[0]):
        for y in range(0, size[1]):
            draw.point((x, y), t2val[(x, y)])

    image.save(filename)

# 去噪处理
def quzao_op(path):
    im = Image.open(path)  # 打开图片
    width = im.size[0]  # 获取宽度
    height = im.size[1]  # 获取长度

    # 去噪操作，字符的像素为(98,114,178) ，除此像素以外都做制白处理
    for x in range(width):
        for y in range(height):
            r, g, b = im.getpixel((x, y))
            if (r,g,b) != obj:
                im.putpixel((x, y), (255, 255, 255))

    im = im.convert('L')  # 二值化处理
    twoValue(im, 120)
    clearNoise(im, 2, 1)
    path1 = "mid.bmp"
    saveImage(path1, im.size)   #  保存去噪二值化的图片
    change_op(path1)  # 转化 bmp格式至jpg
    return path1

# 图片格式转换
def change_op(fileName):
    newFileName = fileName[0:fileName.find("_")] + ".jpg"
    im = Image.open(fileName)
    im.save(newFileName)

def nperoded_op(path):
    img = cv2.imread(path, cv2.COLOR_GRAY2RGB)
    # NumPy定义的结构元素
    NpKernel = np.uint8(np.ones((3, 3)))
    Nperoded = cv2.erode(img, NpKernel)
    # # 显示腐蚀后的图像
    # cv2.imshow("Eroded by NumPy kernel", Nperoded)
    # cv2.save("out.jpg",Nperoded)
    # cv2.imwrite("out.jpg", Nperoded)
    # OpenCV定义的结构元素
    # kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    #
    # # 腐蚀图像
    # eroded = cv2.erode(img, kernel)
    # 显示腐蚀后的图像
    # cv2.imshow("Eroded Image", eroded)
    cv2.imwrite("out.jpg", Nperoded)

# 腐蚀图像 # 4%
def eroded_op(path):
    img = cv2.imread(path, cv2.COLOR_GRAY2RGB)
    # # NumPy定义的结构元素
    # NpKernel = np.uint8(np.ones((3, 3)))
    # Nperoded = cv2.erode(img, NpKernel)
    # # 显示腐蚀后的图像
    # cv2.imshow("Eroded by NumPy kernel", Nperoded)
    # # cv2.save("out.jpg",Nperoded)
    # cv2.imwrite("out.jpg", Nperoded)
    # OpenCV定义的结构元素
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    # 腐蚀图像
    eroded = cv2.erode(img, kernel)
    # 显示腐蚀后的图像
    # cv2.imshow("Eroded Image", eroded)
    cv2.imwrite("out.jpg", eroded)


def Distinguish(path):
    path1 = quzao_op(path) # 去噪处理
    eroded_op(path1) # 腐蚀处理，去噪后的图片需要腐蚀下让图片中的字母更加清晰

    bimage = get_file_content(filePath="out.jpg")

    # 以下时百度云ORC云服务
    # info = client.basicGeneral(bimage, options)  # 4%
    # info = client.basicAccurate(bimage, options)  # 8%
    # info = client.handwriting(bimage, options) # 8 %
    info = client.webImage(bimage, options)  # 8 %
    return info




